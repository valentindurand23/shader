﻿Shader "Unlit/squareCircle"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_SquareColor("Square Color", Color) = (0.25,0,0,1)
		_StripColor("Strip Color", Color) = (1,0,0,1)
		_CircleColor("Circle Color", Color) = (1,0,0,1)
		_BandWidth("Band width", Float) = 5
		_GridScale("Grid Scale", Float) = 1
		_CircleRadius("Circle Radius", Float) = 1
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _SquareColor;
			fixed4 _StripColor;
			fixed4 _CircleColor;
			float _BandWidth;
			float _GridScale;
			float _CircleRadius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				float2 st = frac(i.uv * _GridScale);

				float2 center1 = float2(0, 0);
				float distance1 = distance(st, center1);				
				float2 center2 = float2(1, 0);
				float distance2 = distance(st, center2);				
				float2 center3 = float2(0, 1);
				float distance3 = distance(st, center3);
				float2 center4 = float2(1, 1);
				float distance4 = distance(st, center4);
				 
				float isInCircle1 = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distance1);
				float isInCircle2 = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distance2);
				float isInCircle3 = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distance3);
				float isInCircle4 = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distance4);



				col = lerp(_SquareColor, _StripColor, saturate(step(_BandWidth, abs(st.x - 0.5)) + step(_BandWidth, abs(st.y - 0.5))));
				col = lerp(col, _CircleColor, isInCircle1);
				col = lerp(col, _CircleColor, isInCircle2);
				col = lerp(col, _CircleColor, isInCircle3);
				col = lerp(col, _CircleColor, isInCircle4);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}

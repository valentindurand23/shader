﻿Shader "Unlit/DamierUntilShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_CircleRadius("Circle Radius", Range(0.01,0.5)) = 0.5
		_BandStartColor("Band start Color", Color) = (0,0,1,1)
		_BandEndColor("band end Color", Color) = (0,0,1,1)
		_GridScale("Grid Scale", Range(0,10)) = 2
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _BandEndColor;
				float4 _BandStartColor;
				fixed _CircleRadius;
				float _GridScale;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					// sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);

					float2 center = float2(0.5f, 0.5f);
					float distanceToCenter = distance(i.uv, center);
					float isInCircle = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distanceToCenter);
					float bandWidth = (_CircleRadius * 2) / (float)_GridScale;

					float2 st;
					if (isInCircle < .5)
					{
						st.x = frac(i.uv.x * _GridScale);
						st.y = frac(i.uv.y * _GridScale * 3);
					}
					if (isInCircle > .5)
					{
						st.x = frac(i.uv.x * _GridScale * 3);
						st.y = frac(i.uv.y * _GridScale);
					}

					float position = 0.0;
					if (st.y > center.y && st.x < center.x)
					{
						position = float(1);
					}
					if (st.y < center.y && st.x > center.x)
					{
						position = float(3);
					}
					if (st.y > center.y && st.x > center.x)
					{
						position = float(2);
					}
					if (st.y < center.y && st.x < center.x)
					{
						position = float(4);
					}

					//float position = lerp(st.x, st.y, (step(center.y, i.uv.y) * step(i.uv.x, center.x)) + (step(i.uv.y, center.y) * step(center.x, i.uv.x)));


					float bandID = floor((position - (0.5 - _CircleRadius)) / bandWidth);

					float4 startColor = lerp(_BandStartColor, _BandEndColor, isInCircle);
					float4 endColor = lerp(_BandEndColor, _BandStartColor, isInCircle);

					col = lerp(startColor, endColor, position % 2);

					// apply fog
					UNITY_APPLY_FOG(i.fogCoord, col);
					return col;
				}
				ENDCG
			}
		}
}

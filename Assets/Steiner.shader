﻿Shader "Unlit/Steiner"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Rayon("Rayon" , Float) = 1
		_Number("Number", Int) = 20
		
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                
				fixed4 col = tex2D(_MainTex, i.uv);
				const float PI = 3.14159;
				fixed4 textCol = tex2D(_MainTex, i.uv);
				fixed4 plainCol = fixed4(0.5, 0.5, 0.1, 1);

				fixed2 uvCenter = .5;

				float dist = distance(i.uv, uvCenter);
                // sample the texture

				//R' = (R-R*sin(pi/N))/(sin(pi/N)+1

				col = lerp(plainCol, textCol, smoothstep(0.5, 0.5, (sin( dist) + 1) * 0.49));


                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}

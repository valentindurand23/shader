﻿Shader "Custom/Unlit/MyOpaqueUnlitShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 worldVertex : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				o.worldVertex  = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col;
				fixed4 textCol = tex2D(_MainTex, i.uv);
				fixed4 plainCol = fixed4(0.5,0.5,0.1,1);
				
				fixed2 uvCenter = .5;
				float dist = distance(i.uv, uvCenter);

				//col = lerp(plainCol, textCol,  dist);
				//col = lerp(plainCol, textCol, frac(4*dist));
				//col = lerp(plainCol, textCol, (sin(50*dist)+1)*.5);
				//col = lerp(plainCol, textCol, (sin(50*dist+_Time.y*5)+1)*.5);
				//col = lerp(plainCol, textCol, (sin(50*dist+sin(_Time.y*5)*10)+1)*.5);



				//col = lerp(plainCol, textCol, smoothstep(.25, .75, dist));
				//col = lerp(plainCol, textCol, smoothstep(.25, .75, frac(4 * dist)));
				col = lerp(plainCol, textCol, smoothstep(.25, .75, (sin(50*dist)+1)*.5));
				//col = lerp(plainCol, textCol, smoothstep(.25, .75, (sin(50*dist+_Time.y*5)+1)*.5));
				//col = lerp(plainCol, textCol, smoothstep(.25, .75, (sin(50*dist+sin(_Time.y*5)*10)+1)*.5));

				//col = lerp(plainCol, textCol, smoothstep(.25, .75, i.uv.x));
				//col = lerp(plainCol, textCol,i.uv.x);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
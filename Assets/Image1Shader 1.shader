﻿Shader "Costum/Unlit/Exercice1/Image1"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_BackgroundColor("Background Color", Color) = (0,0,1,1)
		_BandStartColor("Band Start Color", Color) = (0.25,0,0,1)
		_BandEndColor("Band End Color", Color) = (1,0,0,1)
		_CircleRadius("Circle Radius", Range(0.01,1)) = 1
		_NbCircle("Nb Circle", Int) = 2
		_CircleRadiusOrbite("Circle Radius Orbite", Range(0.01,0.5)) = 0.5
		_Color1("Orbite Color 1", Color) = (0,0,0,1)
		_Color2("Orbite Color 2", Color) = (1,1,1,1)
		_X("x", Range(0.01,1)) = 0.5
		_Y("y", Range(0.01,1)) = 0.5
		
		
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _BackgroundColor;
			fixed4 _BandStartColor;
			fixed4 _BandEndColor;
			fixed _CircleRadiusOrbite;
			fixed4 _Color1;
			fixed4 _Color2;
			fixed _CircleRadius;
			fixed _NbCircle;
			fixed _X;
			fixed _Y;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

			float2 center = float2(0.5f, 0.5f);

			float2 centerOrbite1 = float2(_X, _Y);
			float2 centerOrbite2 = float2(1-_X, 1-_Y);

			col = _BackgroundColor;

			float distanceToCenter = distance(i.uv, center);
			float distanceToCenterOrbite1 = distance(i.uv, centerOrbite1);
			float distanceToCenterOrbite2 = distance(i.uv, centerOrbite2);

			float circleSize = _CircleRadius / _NbCircle;
			float CircleID = floor(distanceToCenter / circleSize);

			float isInCircle = 1 - smoothstep(_CircleRadius - 0.001f, _CircleRadius + 0.001f, distanceToCenter);
			float isInCircleOrbite1 = 1 - smoothstep(_CircleRadiusOrbite - 0.001f, _CircleRadiusOrbite + 0.001f, distanceToCenterOrbite1);
			float isInCircleOrbite2 = 1 - smoothstep(_CircleRadiusOrbite - 0.001f, _CircleRadiusOrbite + 0.001f, distanceToCenterOrbite2);

			float4  Colororbite1 = lerp(_Color1, _Color2, step(i.uv.x - i.uv.y, centerOrbite1.x- centerOrbite1.y));
			float4  Colororbite2 = lerp(_Color2, _Color1, step(i.uv.x - i.uv.y, centerOrbite2.x - centerOrbite2.y)) ;

			float4 circleColor = _BandStartColor;

			circleColor = lerp(_BandStartColor, _BandEndColor, CircleID / (_NbCircle - 1));

			col = lerp(col, circleColor, isInCircle);
			col = lerp(col, Colororbite1, isInCircleOrbite1);
			col = lerp(col, Colororbite2, isInCircleOrbite2);
				return col;
			}
			ENDCG
		}
	}
}

﻿Shader "Unlit/DamierUntilShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_BandStartColor("Band start Color", Color) = (0,0,1,1)
		_BandEndColor("band end Color", Color) = (0,0,1,1)
		_GridScale("Grid Scale", Range(0,10)) = 2
		_BandMidColor("band mid Color", Color) = (0,0,1,1)
		
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _BandEndColor;
				float4 _BandStartColor;
				float4 _BandMidColor;
				float _GridScale;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					// sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);

					float2 st;

					st.x = frac(i.uv.x * _GridScale);
					st.y = frac(i.uv.y * _GridScale);

					const float PI = 3.14159;

					if (floor(i.uv.y * _GridScale ) % 2 == 0) {
						st.x += sin(st.y * 1 * PI * 1) * 0.5;
						fixed value = smoothstep(frac(st.x), frac(st.x) + 0.5, 0.55);
						col = lerp(lerp(_BandStartColor, _BandMidColor, smoothstep(1, 0.5, frac(st.x))), lerp(_BandEndColor, _BandMidColor, smoothstep(0.5, 0.5, frac(st.x))), value);
					}

					if (floor(i.uv.y * _GridScale) % 2) {
						st.x -=  sin(st.y * 1 * PI * 1) * 0.5;
						fixed value = smoothstep(frac(st.x), frac(st.x) + 0.5, 0.55);
						col = lerp(lerp(_BandEndColor, _BandMidColor, smoothstep(1, 0.5, frac(st.x))), lerp(_BandStartColor, _BandMidColor, smoothstep(0.5, 0.5, frac(st.x))), value );
					}

					// apply fog
					UNITY_APPLY_FOG(i.fogCoord, col);
					return col;
				}
				ENDCG
			}
		}
}
